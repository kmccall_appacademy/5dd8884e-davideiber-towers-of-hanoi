# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, render the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers
	def initialize
		@towers = [[3,2,1], [], []]
	end

	def render
		puts "What tower do you want to move a disc from?"
		from = gets.chomp.to_i
		puts "What tower do you want to move the disc to?"
		to = gets.chomp.to_i
	end

	def play
		return from, to
		until won?
			from, to = render
			until valid_move?(from, to)
				from, to = render
			end
			move(from, to)
		end
	end

	def won?
		if @towers[0].length == 0
			1.upto(2) do |i|
				if @towers[i].length == 3 && @towers[i] == @towers[i].sort.reverse
					return true
				end
			end
		end
		return false
	end

	def valid_move?(from_tower, to_tower)
		from = @towers[from_tower]
		to = @towers[to_tower]
		unless @towers.include?(from) && @towers.include?(to)
			puts "Choose tower 0, 1, or 2"
			return false
		end

		if from.length > 0 && (to.length == 0 || from[-1] < to[-1])
			return true
		end
		puts "Invalid move"
		return false
	end

	def move(from_tower, to_tower)
		from = @towers[from_tower]
		to = @towers[to_tower]
		to << from.pop
	end
end
